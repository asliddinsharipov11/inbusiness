import {createWebHistory, createRouter}  from "vue-router";



const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            component:()=> import('../pages/Home'),
        },
        {
            path: '/project',
            component:()=> import('../pages/Project'),
        },
        {
            path: '/franch',
            component:()=> import('../pages/Franch'),
        },
        {
            path: '/consult',
            component:()=> import('../pages/Consult'),
        },
        {
            path: '/busniss',
            component:()=> import('../pages/Busniss'),
        },
        {
            path: '/busniss-type',
            component:()=> import('../pages/BusnissType'),
        },
        {
            path: '/register',
            component:()=> import('../pages/Register'),
        },
        {
            path: '/login',
            component:()=> import('../pages/Login'),
        },
        {
            path: '/add-project',
            component:()=> import('../pages/AddProject'),
        },
        {
            path: '/busniss-type-data',
            component:()=> import('../pages/BusnissTypeData'),
        },
        {
            path: '/chat',
            component:()=> import('../pages/Chat'),
        },
       
    ],
  });

export default router;