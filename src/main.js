import { createApp } from 'vue'
import App from './App.vue'
import router from './plugins/router'
import components from '@/components/UI'
import VueSSE from 'vue-sse';



const app = createApp(App)
components.forEach(component => {
    app.component(component.name,component)
});
app.use(VueSSE)
app.use(router)
app.mount('#app')
